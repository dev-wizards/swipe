# SWIPER

##Requirements
php-gd php-mbstring php-zip php-mcrypt php-mysql

##Tested on
PHP 7.1
Should work on PHP > 5.6


1. Point your webserver to `/public` dir
2. Configure access to your database in `.env` file
3. Run `/database/photos.table.dump.sql` to create DB table
4. Run `composer update` if something goes wrong. 

Photos dir: `/public/photos`
Original: `/public/photos/original`
Resized: `/public/photos/iphone` - resize, based on iPhone resolution

