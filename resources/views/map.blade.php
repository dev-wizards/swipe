@extends('layout')

@section('main')
    <a href="{!! route('index') !!}" id="back" class="btn btn-md btn-primary btn-map" style="display: none">
        <span class="glyphicon glyphicon-arrow-left"></span> Back</a>

    <script>

        $(function () {

            var myLatlng = new google.maps.LatLng({!! $photo->lat !!}, {!! $photo->lng !!});
            var mapOptions = {
                zoom: 12,
                center: myLatlng,
                disableDefaultUI: true

            }
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            // Place a draggable marker on the map
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
//            draggable: true,
//            title: "Drag me!"
            });

            map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById("back"));

            google.maps.event.addListenerOnce(map, 'idle', function () {
                $("#back").fadeIn(100);
            });

        })();
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzHT8FHOLdWbUMScePYrMCzIe42ETYIgQ&callback=initMap"></script>

    <section class="map" id="map"></section>

@endsection