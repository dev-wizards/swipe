@if($photos)
    @include('navbar')
@endif

@if(count($photos))
    <!-- Slider main container -->
    <div class="swiper-container">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->

            @foreach($photos as $card)
                @include("card", $card)
            @endforeach


        </div>
    </div>
@else
    <div class="container text-center">
        <p style="margin-top: 45vh; color: white;">We have no cards yet, <a class="btn btn-primary" href="{!! route('photos.create') !!}"><span class="glyphicon glyphicon-circle-arrow-right"></span> Upload</a> right
            now!</p>
    </div>
@endif
<script>
    var page = 1;

    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: false,
        onReachEnd: function(swiper) {
            $.getJSON("{!! route('photos.json') !!}?page="+page, function(data){
                if(data.length)
                    swiper.appendSlide(data);
            });
            page++;
        },
        onTransitionEnd: function(swiper) {
            var lat = $(".swiper-slide-active").data("lat");
            var lng = $(".swiper-slide-active").data("lng");
            if(lat != "" && lng !="") {
                $("#mapLink").parent().show();
                $("#mapLink").attr("href", "/map/" + $(".swiper-slide-active").data("id"));
            } else {
                $("#mapLink").parent().hide();
            }
        },
        onTap: function(swiper, event) {
            $(".swiper-slide-active .description").slideToggle("fast");
        }
    });

    mySwiper.onTransitionEnd(mySwiper);


</script>