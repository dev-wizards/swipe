<div class="swiper-slide" data-lat="{!! $card->lat !!}" data-lng="{!! $card->lng !!}" data-id="{!! $card->id !!}">
    <section class="card">
        <div class="image"
             style="background: url('/photos/iphone/{!! $card->filename !!}') top center;">
            <div class="info container-fluid">
                <h1>{{ $card->title }}</h1>
                <p class="col-xs-12 description" style="display:none">{{ $card->description }}</p>
                @if(isset($card->lat, $card->lng))
                    <!-- <a href="{!! route("photo.map", $card) !!}" class="col-xs-2 geolocation-pin text-center">
                        <span class="glyphicon glyphicon-map-marker"></span>
                    </a> -->
                @endif
            </div>
        </div>
    </section>
</div>